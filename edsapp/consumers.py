from django.http import HttpResponse
from channels import Group

def ws_order_connect(message):
    message.reply_channel.send({"accept": True})
    Group("orders").add(message.reply_channel)

def ws_item_connect(message):
    message.reply_channel.send({"accept": True})
    Group("items").add(message.reply_channel)

def ws_order_change(message):
    print ("here")
    message.reply_channel.send({
        "text": "order_change",
    })

def ws_item_change(message):
    message.reply_channel.send({
        "text": "item_change",
    })

def ws_order_disconnect(message):
    Group("orders").discard(message.reply_channel)

def ws_item_disconnect(message):
    Group("orders").discard(message.reply_channel)
