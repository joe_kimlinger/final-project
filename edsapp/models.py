# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from datetime import datetime
from django.core.validators import RegexValidator

from django.db import models
from django.dispatch import receiver
from django.db.models.signals import post_save

# Create your models here.
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Please enter phone number in the format: '+999999999'. Up to 15 digits allowed.")
    phone_number = models.CharField(validators=[phone_regex], max_length=17, blank=True)

class Item(models.Model):
    name = models.CharField(max_length=100, blank=False)
    orders = models.IntegerField(default=0)
    price = models.DecimalField(max_digits=5, decimal_places=2)
    inStock = models.BooleanField(default=True)

class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created = models.DateTimeField(null=True,default=None)
    last_used = models.DateTimeField(null=True,default=None)
    status = models.CharField(max_length=50, default='inProgress')
    total = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    used = models.IntegerField(default=0)
    name = models.CharField(max_length=50, blank=False)

class OrderItem(models.Model):
    order = models.ForeignKey(Order, null=True, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=0)

    def __str__(self):
        return '%s %s' % (self.item.name, self.quantity)

@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()
