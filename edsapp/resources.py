from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from edsapp.models import *

class OrderResource(ModelResource):
    class Meta:
        queryset = Order.objects.all()
        resource_name = "order"

        filtering = {
            'user': ALL_WITH_RELATIONS,
        }

class OrderItemResource(ModelResource):
    class Meta:
        queryset = OrderItem.objects.all()
        resource_name = "order_item"

        filtering = {
            'order': ALL_WITH_RELATIONS,
        }
