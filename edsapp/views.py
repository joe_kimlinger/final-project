# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse
from django.shortcuts import redirect

from django.views.generic import UpdateView, CreateView, TemplateView, ListView, DetailView, View
from django.contrib.auth.views import LoginView, LogoutView
from edsapp.models import *

from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth.mixins import UserPassesTestMixin

from django.contrib import messages
from django.shortcuts import render, redirect
from django.contrib.auth import models 
from django.shortcuts import get_object_or_404
from django.core import serializers
import json

from edsapp.forms import *
from django.forms import formset_factory

from django.db.models import Q
from django.middleware import csrf

from channels import Group



class RegistrationView(CreateView):
    form_class = RegistrationForm
    template_name = "signup.html"

    def form_valid(self, form):
	user = User.objects.create_user(form.cleaned_data.get('username'), password=form.cleaned_data.get('password1'))
        profile = Profile(user=user, phone_number=form.cleaned_data.get('phone_number'))
	user.profile = profile
        user = authenticate(username=form.cleaned_data.get('username'), password=form.cleaned_data.get('password1'))
        g = models.Group.objects.get(name='Customers')
        g.user_set.add(user)
        if user.is_authenticated():
            login(self.request, user)
            return redirect('/')


class CustomLoginView(LoginView):
    template_name = "login.html"

    def get_success_url(self):
        messages.success(self.request, 'You have been logged in')
        self.request.session["cart"] = {}
        return self.request.GET.get('next', "/")


class CustomLogoutView(LogoutView):
    def get_next_page(self):
        messages.warning(self.request, 'You have been logged out')
        return self.request.GET.get('next', "/")


class ProfileUpdateView(LoginView, UpdateView):
    form_class = UpdateProfileForm
    template_name = "profile_update.html"
    context_object_name = "user"

    def get_object(self):
        return User.objects.all().filter(pk=self.request.user.pk).first()

    def get_context_data(self, **kwargs):
        context = super(ProfileUpdateView, self).get_context_data(**kwargs)
        return context


class ProfileView(LoginView, DetailView):
    template_name = "profile.html"
    model = User
    context_object_name = "user"


class EditProfileView(LoginView, DetailView):
    template_name = "edit_profile.html"
    model = User
    context_object_name = "user"

    def post(self, request, *args, **kwargs):
        user = User.objects.all().filter(pk=self.request.user.pk).first()
        user.profile.phone_number=self.request.POST.get('phone_number')
        user.save()
        user.profile.save()
        return HttpResponseRedirect(reverse('home'))


class HomeView(LoginView, ListView):
    model = Order
    template_name = "homepage.html"
    context_object_name = "orders"

    def get_queryset(self):
        if self.request.user.is_active:
            q = super(HomeView, self).get_queryset().filter(user=self.request.user)
            q = q.filter(Q(status='placed') | Q(status='preparing') | Q(status='ready'))
            return q.order_by('-last_used');
        else:
            return None

    def get_context_data(self, **kwargs):
        c = super(HomeView, self).get_context_data(**kwargs)
        c['items'] = OrderItem.objects.all()
        return c


class InventoryView(UserPassesTestMixin, ListView):
    model = Item
    template_name = "inventory.html"
    context_object_name = "items"

    def test_func(self):
        return self.request.user.groups.filter(name='Employees').exists()

    def get_queryset(self):
        q = super(InventoryView, self).get_queryset()
        return q.order_by('-name');

    def post(self, request, *args, **kwargs):
        items = self.get_queryset()
        for item in items:
            if item.name == self.request.POST.get('itemName'):
                if item.inStock:
                    item.inStock = False
                else:
                    item.inStock = True
                item.save()
                Group("items").send({"text": "update"})
        return HttpResponseRedirect(reverse('inventory'))


class OrderQueueView(UserPassesTestMixin, ListView):
    model = Order
    template_name = "order_queue.html"
    context_object_name = "orders"

    def test_func(self):
        return self.request.user.groups.filter(name='Employees').exists()

    def get_queryset(self):
        q  = Order.objects.all().filter(Q(status='placed') | Q(status='preparing') | Q(status='ready'))
        return q.order_by('last_used')

    def get_context_data(self, **kwargs):
        c = super(OrderQueueView, self).get_context_data(**kwargs)
        c['items'] = OrderItem.objects.all()
        return c

    def post(self, request, *args, **kwargs):
        order = Order.objects.filter(id=self.request.POST.get('orderId')).first()
        action = self.request.POST.get('action')
        if action == 'start':
            order.status = 'preparing'
        elif action == 'clear' or action == 'finish':
            order.status = 'completed'
        elif action == 'ready':
            order.status = 'ready'
        order.save()
        Group("orders").send({"text": str(order.user.id)})
        return HttpResponseRedirect(reverse('order_queue'))


class PastOrderView(LoginRequiredMixin, ListView):
    model = Order
    template_name = "past_orders.html"
    context_object_name = "orders"

    def get_queryset(self):
        q  = Order.objects.all().filter(status='completed', user=self.request.user)
        return q.order_by('-last_used')

    def get_context_data(self, **kwargs):
        c = super(PastOrderView, self).get_context_data(**kwargs)
        c['items'] = OrderItem.objects.all()
        temp = Order.objects.all().filter(user=self.request.user, status='completed')
        c['most_common'] = temp.order_by('-used')[:3]
        return c

    def post(self, request, *args, **kwargs):
        order = Order.objects.filter(id=self.request.POST.get('orderid')).first()
        if self.request.POST.get('action') == 'order':
            order.status = 'placed'
            order.used = order.used + 1
            order.last_used = datetime.now()
            self.request.session['order_num'] = None
            order.save()
            messages.success(self.request, "Order placed")
            Group("orders").send({"text": str(request.user.id)})
            return HttpResponseRedirect(reverse('home'))
        elif self.request.POST.get('action') == 'remove':
            delete_order(order)
            return HttpResponseRedirect(reverse('past_order'))
        elif self.request.POST.get('action') == 'remove_all':
            for order in Order.objects.filter(user=self.request.user):
                delete_order(order)
            return HttpResponseRedirect(reverse('past_order'))


def delete_order(order):
    for item in OrderItem.objects.filter(order=order):
        item.delete()
    order.delete()


class OrderView(LoginRequiredMixin, ListView):
    model = Order
    template_name = "order.html"
    context_object_name = "order"

    def get_context_data(self, **kwargs):
        if 'order_num' not in self.request.session.keys() or not self.request.session['order_num']:
            order = Order(user=self.request.user, created=datetime.now())
            order.save()
            self.request.session['order_num'] = order.id
        else:
            order = Order.objects.filter(id=self.request.session['order_num']).first()
            if not order:
                order = Order(user=self.request.user)
                self.request.session['order_num'] = order.id

        q = super(OrderView, self).get_context_data(**kwargs)
        q['menu'] = Item.objects.filter(inStock=True).order_by('name')
        items = OrderItem.objects.filter(order=order)
        q['items'] = items
        total = 0
        for item in items:
            total += item.item.price * item.quantity
        order.total = total
        order.save()
        q['order'] = order
        return q


    def post(self, request, *args, **kwargs):
        base = Item.objects.filter(name=self.request.POST.get('itemName')).first()
        order = Order.objects.filter(id=self.request.session['order_num']).first()
        item = OrderItem.objects.filter(order=order, item=base).first()
        if self.request.POST.get('action') == 'add':
            if item:
                item.quantity += 1
            else:
                item = OrderItem(order=order, item=base, quantity=1)
            item.save()
            return HttpResponseRedirect(reverse('order'))
        elif self.request.POST.get('action') == 'remove':
            if item and item.quantity > 1:
                item.quantity -= 1
                item.save()
            else:
                OrderItem.objects.filter(order=order, item=base).delete()
            return HttpResponseRedirect(reverse('order'))
        elif self.request.POST.get('action') == 'order':
            order.status = 'placed'
            order.total = self.request.POST.get('total')
            order.used = order.used + 1
            order.last_used = datetime.now()
	    order.name = self.request.POST.get('orderName')
            self.request.session['order_num'] = None
            order.save()
            messages.success(self.request, "Order placed")
            Group("orders").send({"text": str(request.user.id)})
            return HttpResponseRedirect(reverse('home'))


class APILogin(LoginView):
    def get(self, request, *args, **kwargs):
        resp = HttpResponse("Token obtained")
        csrf.get_token(request)
        resp.set_cookie("csrftoken", csrf.get_token(request))
        return resp

    def post(self, request, *args, **kwargs):
        result = {'result': 'success'}
        user = authenticate(username=request.POST.get('username'), password=request.POST.get('password'))
        if user:
            login(request, user)
        else:
            result['result'] = 'error'
        return JsonResponse(result)


class APILogout(LogoutView):
    def get(self, request, *args, **kwargs):
        logout(request)

        result = {'result': 'success'}
        if self.request.user.is_authenticated:
            result['result'] = 'error'
        return JsonResponse(result)


class APIOrder(LoginRequiredMixin, DetailView):

    def get_queryset(self, **kwargs):
        q  = OrderItem.objects.all().filter(order=self.kwargs['pk'])
        return q

    def get(self, request, *args, **kwargs):
        q = self.get_queryset()
        r = {}
        r["total"] = float(Order.objects.all().filter(pk=self.kwargs['pk']).first().total)
	r["name"] = str(Order.objects.all().filter(pk=self.kwargs['pk']).first().name)

        r["items"] = {}
        for x in q:
            r["items"][x.item.name] = x.quantity
        return HttpResponse(json.dumps(r))

    def delete(self, request, *args, **kwargs):
        order = Order.objects.filter(pk=self.kwargs['pk']).first()
        delete_order(order)
        return HttpResponse(json.dumps({"result": "success"}))

    def put(self, request, *args, **kwargs):
        order = Order.objects.all().filter(pk=self.kwargs['pk']).first()
        order.status = 'placed'
        order.used = order.used + 1
        order.last_used = datetime.now()
        order.save()
        Group("orders").send({"text": str(request.user.id)})
        return HttpResponse(json.dumps({"result": "success"}))


class APINewOrder(LoginRequiredMixin, DetailView):

    def post(self, request, *args, **kwargs):
        order = Order(user=request.user, created=datetime.now())
        order.save()
        total = 0
        for i in request.POST.keys():
	    if i == "orderName":
		order.name = request.POST[i]
		continue
            item = Item.objects.all().filter(name=i).first()
            if item:
                new_item = OrderItem(order=order, item=item, quantity=request.POST[i])
                new_item.save()
                total += int(new_item.quantity) * item.price
        order.status = 'placed'
        order.total = total
        order.used = 1
        order.last_used = datetime.now()
        order.save()
        Group("orders").send({"text": str(request.user.id)})
        return HttpResponse(json.dumps({"result": "success", "order_id": order.id}))


class APIPastOrders(LoginRequiredMixin, ListView):
    def get_queryset(self):
        q  = Order.objects.all().filter(status='completed', user=self.request.user)
        return q.order_by('-last_used')

    def get(self, request, *args, **kwargs):
        result = []
        for order in self.get_queryset():
            d = {}
            d["created"] = str(order.created)
            d["last_used"] = str(order.last_used)
            d["total"] = float(order.total)
            d["used"] = int(order.used)
            d["id"] = int(order.id)
	    d["items"] = []
	    d["name"] = str(order.name)
	    q = OrderItem.objects.all().filter(id=order.id) 
            for x in q:
		item = {}
                item["name"] = x.item.name
		item["quantity"] = x.quantity
		item["price"] = float(x.item.price)
		d["items"].append(item)
            result.append(d)
        return HttpResponse(json.dumps(result))
