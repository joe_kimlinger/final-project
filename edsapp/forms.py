from django import forms
from edsapp.models import User, Profile

from django.contrib.auth.forms import UserCreationForm, UserChangeForm

class RegistrationForm(UserCreationForm):
    phone_number = forms.CharField()

    class Meta:
        model = User
        fields = ('username', 'email', )

class UpdateProfileForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(UpdateProfileForm, self).__init__(*args, **kwargs)

    first_name = forms.CharField(label='First Name')
    last_name = forms.CharField(label='Last Name')
    phone_number = forms.CharField(label='Phone Number')

    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name')
