"""eds URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from edsapp.views import *
from edsapp.resources import *
from tastypie.api import Api


api = Api(api_name='v1')
api.register(OrderResource())
api.register(OrderItemResource())


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/', CustomLoginView.as_view(), name="login"),
    url(r'^accounts/login/', CustomLoginView.as_view(), name="login"),
    url(r'^logout/$', CustomLogoutView.as_view(next_page="/"), name="logout"),
    url(r'^signup/$', RegistrationView.as_view(), name="signup"),
    url(r'^profile_edit/(?P<pk>\d+)$', EditProfileView.as_view(), name="profile_edit"),
    url(r'^inventory/', InventoryView.as_view(), name="inventory"),
    url(r'^order_queue/', OrderQueueView.as_view(), name="order_queue"),
    url(r'^order/', OrderView.as_view(), name="order"),
    url(r'^past_orders/', PastOrderView.as_view(), name="past_order"),
    url(r'^api/login/$', APILogin.as_view(), name='api_login'),
    url(r'^api/logout/$', APILogout.as_view(), name='api_logout'),
    url(r'^api/order/(?P<pk>\d+)$', APIOrder.as_view(), name='api_order'),
    url(r'^api/order/$', APINewOrder.as_view(), name='api_new_order'),
    url(r'^api/past_orders/$', APIPastOrders.as_view(), name='api_past_orders'),
    url(r'^api/', include(api.urls)),
    url(r'^$', HomeView.as_view(), name="home")
]
