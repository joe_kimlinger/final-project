from channels.routing import route
from edsapp.consumers import *

channel_routing = [
    route("websocket.connect", ws_order_connect, path=r'/orders/$'),
    route("websocket.connect", ws_item_connect, path=r'/items/$'),
    route("websocket.receive", ws_order_change, path=r'/orders/$'),
    route("websocket.recieve", ws_item_change, path=r'/items/$'),
    route("websocket.disconnect", ws_order_disconnect, path=r'/orders/$'),
    route("websocket.disconnect", ws_item_disconnect, path=r'/items/$'),
]
